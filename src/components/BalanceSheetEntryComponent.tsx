import React, {useCallback, useEffect, useState} from 'react';

import NumberFormat from 'react-number-format';
import debounce from 'lodash/debounce';

import BalanceSheetEntry from '../types/balanceSheetEntry';
import Currency from '../types/currency';




interface IBalanceSheetEntryProps {
    entry: BalanceSheetEntry,
    onUpdateEntry: (updatedEntry: BalanceSheetEntry) => any,
    onDeleteEntry: (deletedEntry: BalanceSheetEntry) => any,
    currency: Currency | undefined

}

const BalanceSheetEntryComponent: React.FC<IBalanceSheetEntryProps> = ({ entry, onUpdateEntry, onDeleteEntry, currency }) => {
    const formatterOptions = {
        style:'decimal',
        maximumFractionDigits: 2,
        minimumFractionDigits: 2
    };
    const amountFormatter = new Intl.NumberFormat(['en-CA', 'en-US', 'en-GB', 'fr-CA', 'en', 'fr'], formatterOptions);

    const [formattedAmount, setFormattedAmount] = useState(amountFormatter.format(entry.amount));

    useEffect(()=>{
        setFormattedAmount(amountFormatter.format(entry.amount));
    }, [entry, amountFormatter]);

    const onChangeAmount = useCallback(debounce(values => {
        const {value} = values;
        onUpdateEntry({
            ...entry,
            amount: value
        });
    }, 500), [entry, onUpdateEntry]);


    return (
        <div className={'flex w-100  items-end'}>
            <div className={entry.monthly ? 'w-third ' : 'w-two-thirds'}>{entry.description}</div>
            { entry.monthly && <div className={'flex w-third items-end monthly-amount'}>
                <div className={'ph2 w-10'}>{ entry.monthly && currency && currency.symbol }</div>
                <div className={'tr w-90'}>
                    { entry.monthly && amountFormatter.format(entry.monthly) }
                </div>
            </div> }
            <div className={'flex w-third sub-total-amount'}>
                <div className={'ml3'}>{ currency && currency.symbol }</div>
                <NumberFormat
                              className={'sub-total-amount w-90 tr ba b--white'}
                              thousandSeparator={','}
                              value={formattedAmount}
                              decimalScale={2}
                              fixedDecimalScale={true}
                              isNumericString={true}
                              onValueChange={onChangeAmount}
                />
            </div>
            <button className="f7 dim ba ph1 mb1 dib dark-blue bg-white" onClick={()=> onDeleteEntry(entry)}>&times;</button>
        </div>

    );
};

export default BalanceSheetEntryComponent;