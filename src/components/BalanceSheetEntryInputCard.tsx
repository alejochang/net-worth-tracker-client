import React, {useCallback, useEffect, useState} from 'react';
import NumberFormat from "react-number-format";

import BalanceSheetEntry from '../types/balanceSheetEntry';
import BalanceSheetEntryType, {getTypedEntryType} from '../types/balanceSheetEntryType';
import BalanceSheetEntryCategory, {getTypedEntryCategory} from '../types/balanceSheetEntryCategory';
import Currency, {USD} from '../types/currency';

import * as en from '../en.json';

interface IBalanceSheetEntryCardProps {
    onCloseModal: (category: BalanceSheetEntryCategory, type: BalanceSheetEntryType,  entry?: BalanceSheetEntry) => any,
    currency?: Currency

}

const BalanceSheetEntryInputCard: React.FC<IBalanceSheetEntryCardProps> = ({onCloseModal, currency}) => {
    const entryPlaceholder: BalanceSheetEntry = {
        amount: 0,
        monthly: 0,
        description:'',
        currency: currency ? currency.isoCode : USD.isoCode
    };

    const [newEntry, setNewEntry] = useState(entryPlaceholder);

    const [description, setDescription] = useState(newEntry.description);
    const [formattedAmount, setFormattedAmount] = useState(newEntry.amount);
    const [formattedMonthly, setFormattedMonthly] = useState(newEntry.monthly);
    const [currentCategory, setCurrentCategory] = useState(BalanceSheetEntryCategory.ASSET);
    const [currentType, setCurrentType] = useState(BalanceSheetEntryType.SHORT_TERM);

    const onClick= useCallback((entry?: BalanceSheetEntry) => {
        onCloseModal(currentCategory, currentType, entry);
    }, [onCloseModal, currentType, currentCategory]);

    const onChangeDescription= useCallback(({target}) => {
        setDescription(target.value);
    }, [setDescription]);

    const onChangeAmount = useCallback(values => {
        const {value} = values;
        setFormattedAmount(value);
    }, [setFormattedAmount]);

    const onChangeMonthly = useCallback(values => {
        const {value} = values;
        setFormattedMonthly(value);
    }, [setFormattedMonthly]);

    const onChangeCategory = useCallback(({target})  => {
        const category = getTypedEntryCategory(target.value);
        setCurrentCategory(category);
    }, [setCurrentCategory]);

    const onChangeType = useCallback(({target})  => {
        const type = getTypedEntryType(target.value);
        setCurrentType(type);
    }, [setCurrentType]);


    useEffect(() => {
        setNewEntry({
            amount: formattedAmount,
            monthly: formattedMonthly,
            description: description,
            currency: currency ? currency.isoCode : USD.isoCode,
            type: currentType
        })
    },[description, formattedAmount, formattedMonthly, currency, setNewEntry, currentType]);

    return (
        <div className="modal_content">
           <div className={'flex w-100  items-end pv2'}>
                <div className={'w-40'}>
                    <select className={'w-100 tr mv1 tr'} onChange={onChangeCategory}>
                        {
                            Object.keys(BalanceSheetEntryCategory).map(category =>
                                <option value={category}>{category}</option>
                            )
                        }
                    </select>
                </div>
                <div className={'w-40'}>
                    <select className={'w-100 tr mv1 tr'} onChange={onChangeType}>
                        {
                            Object.keys(BalanceSheetEntryType).map(type =>
                                <option value={type}>{type}</option>
                            )
                        }
                    </select>
                </div>
                <div className={'w-20 monthly-amount tr pv1'}>
                    {currency && `${currency.symbol} ${currency.isoCode}`}
                </div>
            </div>
            <div className={'flex w-100  items-end'}>
                <div className={'w-40 monthly-amount tl'}>{en.description}</div>
                <div className={'w-30 monthly-amount tl'}>
                    {en.monthlyPayment}
                </div>

                <div className={'w-30 monthly-amount tl'}>
                    {en.amount}
                </div>
            </div>
            <div className={'flex w-100  items-end'}>
                <div className={'w-40'}><input className={'w-100'} value={description} onChange={onChangeDescription}/> </div>
                <div className={'w-30 monthly-amount'}>
                    <NumberFormat
                        className={'tr w-100'}
                        thousandSeparator={','}
                        value={formattedMonthly}
                        decimalScale={2}
                        fixedDecimalScale={true}
                        isNumericString={true}
                        onValueChange={onChangeMonthly}
                    />
                </div>

                <div className={'flex w-30 monthly-amount'}>
                    <NumberFormat
                        className={'tr w-100'}
                        thousandSeparator={','}
                        value={formattedAmount}
                        decimalScale={2}
                        fixedDecimalScale={true}
                        isNumericString={true}
                        onValueChange={onChangeAmount}
                    />
                </div>
            </div>
            <span className={'add pv1 dim ba bw1 mv2 mh2 dib green-app border-green-app bg-white w-10 tc'} onClick={() => onClick(newEntry)}>{en.add}</span>
            <span className={'close pv1 dim ba bw1 mv2 mh2 ph1 dib red-app border-red-app bg-white w-10 tc'} onClick={() => onClick()}>{en.cancel}</span>
        </div>
    );

}

export default BalanceSheetEntryInputCard;