import React from 'react';
import Currency from '../types/currency';



interface IBalanceSheetTotalProps {
    description: string,
    currency?: Currency,
    amount: number

}

const BalanceSheetTotalComponent: React.FC<IBalanceSheetTotalProps> = ({ description, amount , currency}) => {
    const amountFormatter = new Intl.NumberFormat(['en-CA', 'en-US', 'fr-CA'], {
                                                                                                style:'decimal',
                                                                                                maximumFractionDigits: 2,
                                                                                                minimumFractionDigits: 2
                                                                                                });

    return (
        <div className={'flex w-100  items-end total-row'}>
            <div className={`w-two-thirds`}>{description}</div>
            <div className={'flex w-third items-end'}>
                <div className={`ph1 w-10`}>{ currency && currency.symbol }</div>
                <div className={`tr w-100`}>{ amountFormatter.format(amount) }</div>
            </div>
        </div>

    );
};

export default BalanceSheetTotalComponent;