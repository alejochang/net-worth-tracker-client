import React, {useCallback, useEffect, useState} from 'react';

import serviceApi from '../services/serviceApi';

import BalanceSheet from '../types/balanceSheet';
import Currency, {USD} from '../types/currency';
import BalanceSheetEntryType from '../types/balanceSheetEntryType';
import BalanceSheetEntry from '../types/balanceSheetEntry';
import BalanceSheetEntryCategory from '../types/balanceSheetEntryCategory';

import BalanceSheetEntryComponent from './BalanceSheetEntryComponent';
import BalanceSheetTotalComponent from './BalanceSheetTotalComponent';
import BalanceSheetEntryInputCard from './BalanceSheetEntryInputCard';
import {loadBalanceSheet} from '../utils/balanceSheetUtils';

import * as en from '../en.json';

interface IBalanceSheetProps {
    balanceSheet: BalanceSheet,
    currencies: Currency[]
};

const BalanceSheetComponent: React.FC<IBalanceSheetProps> = ({balanceSheet, currencies}) => {
    const [shortTermAssets, setShortTermAssets] = useState<BalanceSheetEntry[]>([]);
    const [longTermAssets, setLongTermAssets] = useState<BalanceSheetEntry[]>([]);
    const [shortTermLiabilities, setShortTermLiabilities] = useState<BalanceSheetEntry[]>([]);
    const [longTermLiabilities, setLongTermLiabilities] = useState<BalanceSheetEntry[]>([]);
    const [totalNetWorth, setTotalNetWorth] = useState<number>(0);
    const [totalAssets, setTotalAssets] = useState<number>(0);
    const [totalLiabilities, setTotalLiabilities] = useState<number>(0);
    const [currency, setCurrency] = useState<Currency | undefined>(USD);
    const [baseCurrency, setBaseCurrency] = useState<Currency | undefined>(USD);

    const [addEntry, setAddEntry] = useState(false);

    const updateBalanceSheet = useCallback( ({
                                                 shortTermAssets,
                                                 longTermAssets,
                                                 shortTermLiabilities,
                                                 longTermLiabilities,
                                                 currency
                                             }) => {
        setShortTermAssets(shortTermAssets);
        setLongTermAssets(longTermAssets);
        setShortTermLiabilities(shortTermLiabilities);
        setLongTermLiabilities(longTermLiabilities);
        setCurrency(currency);
        setBaseCurrency(currency);
    }, [setShortTermAssets, setLongTermAssets, setShortTermLiabilities, setLongTermLiabilities, setCurrency, setBaseCurrency]);

    useEffect(() => {
        const processedBalanceSheet = loadBalanceSheet(balanceSheet, currencies);
        updateBalanceSheet(processedBalanceSheet);
    }, [balanceSheet, currencies, updateBalanceSheet]);

    useEffect(() => {
        (async () => {
            try {
                const totals = await serviceApi.calculateNetWorth({
                    assets: [...shortTermAssets, ...longTermAssets],
                    liabilities: [...shortTermLiabilities, ...longTermLiabilities],
                    currency: currency ? currency.isoCode : USD.isoCode
                });
                const {assetsTotal, liabilitiesTotal, netWorthTotal} = totals;
                setTotalAssets(assetsTotal);
                setTotalLiabilities(liabilitiesTotal);
                setTotalNetWorth(netWorthTotal);

            }catch (e) {
                console.error(e);
                setTotalAssets(0);
                setTotalLiabilities(0);
                setTotalNetWorth(0);
            }
        })();
    }, [shortTermAssets, longTermAssets, shortTermLiabilities, longTermLiabilities, currency]);


    const callExchange = useCallback(async (tradeCurrency?: Currency) => {
            if(tradeCurrency){
                try {
                    const convertedBalanceSheet = await serviceApi.convertBalanceSheet({
                        assets: [...shortTermAssets, ...longTermAssets],
                        liabilities: [...shortTermLiabilities, ...longTermLiabilities],
                        currency: baseCurrency ? baseCurrency.isoCode : USD.isoCode
                    }, tradeCurrency.isoCode);
                    const balanceSheet = loadBalanceSheet(convertedBalanceSheet, currencies);
                    updateBalanceSheet(balanceSheet);

                }catch (e) {
                    console.error(e);
                    loadBalanceSheet({
                        assets: [...shortTermAssets, ...longTermAssets],
                        liabilities: [...shortTermLiabilities, ...longTermLiabilities],
                        currency: USD.isoCode
                    }, currencies);
                }
            }
    }, [shortTermAssets, longTermAssets, shortTermLiabilities, longTermLiabilities, updateBalanceSheet, currencies, baseCurrency]);

    const onSelectCurrency = useCallback(async({target}) => {
        const selectedCurrency = currencies.find(currency => currency.isoCode === target.value);
        setCurrency(selectedCurrency);
        await callExchange(selectedCurrency);
    }, [setCurrency, currencies, callExchange]);

    const notifyEntryUpdate = (updatedEntry: BalanceSheetEntry, sourceEntries: BalanceSheetEntry[], setSourceEntries: (entries: BalanceSheetEntry[]) => void) => {
        if(updatedEntry){
            const updatedEntries = sourceEntries.map(entry => {
                if(entry.description === updatedEntry.description){
                    return {
                        ...entry,
                        ...updatedEntry
                    };
                }else{
                    return entry;
                }
            });
            setSourceEntries(updatedEntries);
        }
    };

    const notifyEntryDelete = (deletedEntry: BalanceSheetEntry, sourceEntries: BalanceSheetEntry[], setSourceEntries: (entries: BalanceSheetEntry[]) => void) => {
        if(deletedEntry){
            const updatedEntries = sourceEntries.filter(entry => entry.description !== deletedEntry.description);
            setSourceEntries(updatedEntries);
        }
    };

    const appendEntry = (newEntry: BalanceSheetEntry, sourceEntries: BalanceSheetEntry[], setSourceEntries: (entries: BalanceSheetEntry[]) => void) => {
        if(newEntry){
            const updatedEntries = [...sourceEntries, newEntry];
            setSourceEntries(updatedEntries);
        }
    };

    const onCloseModalEntryInput = useCallback((category, type, entry) => {
        if (entry){
            if(category === BalanceSheetEntryCategory.ASSET){
                if(type === BalanceSheetEntryType.SHORT_TERM){
                    appendEntry(entry, shortTermAssets, setShortTermAssets);
                }else if(type === BalanceSheetEntryType.LONG_TERM){
                    appendEntry(entry, longTermAssets, setLongTermAssets);
                }
            }else if(category === BalanceSheetEntryCategory.LIABILITY) {
                if(type === BalanceSheetEntryType.SHORT_TERM){
                    appendEntry(entry, shortTermLiabilities, setShortTermLiabilities);
                }else if(type === BalanceSheetEntryType.LONG_TERM){
                    appendEntry(entry, longTermLiabilities, setLongTermLiabilities);
                }
            }

        }
        setAddEntry(false);
    }, [
        shortTermAssets,
        setShortTermAssets,
        longTermAssets,
        setLongTermAssets,
        shortTermLiabilities,
        setShortTermLiabilities,
        longTermLiabilities,
        setLongTermLiabilities,
        setAddEntry]);

    return (
        <div className={'flex-column w-50-l w-75-m w-100-s center balance-sheet'}>

            <div className={'flex w-100 items-end pv4'}>
                <div className={'w-50'}>
                    <button className={'dim ba bw1 ph2 pv1 mb1 dib green-app border-green-app bg-white'} onClick={() => setAddEntry(true)}>{en.addEntry}</button>
                    {addEntry && <BalanceSheetEntryInputCard onCloseModal={onCloseModalEntryInput} currency={currency}/>}
                </div>
                <label className={'w-30 tr pv1 mr1'} htmlFor="currency">{en.selectCurrency}:</label>
                <select id={'currency'} className={'w-20 mv1'} onChange={onSelectCurrency}>
                    {
                        currencies.map(currency => (
                            <option key={currency.isoCode} value={currency.isoCode}>{`${currency.symbol} ${currency.isoCode}`}</option>
                        ))
                    }
                </select>
            </div>

            <BalanceSheetTotalComponent
                key={'total-net-worth'}
                description={en.netWorth}
                amount={totalNetWorth}
                currency={currency}
            />
            <br/>
            <div className={'flex w-100 items-end total-row'}>
                {en.assets}
            </div>
            <div className={'flex w-100 items-end sub-group-row'}>
                {en.cashAndInvestments}
            </div>
            {
                shortTermAssets && shortTermAssets.map(
                    entry => {
                        return (
                            <BalanceSheetEntryComponent key={entry.description} entry={entry} currency={currency}
                                onUpdateEntry={ updatedEntry => {
                                    notifyEntryUpdate(updatedEntry, shortTermAssets, setShortTermAssets);
                                }}
                                onDeleteEntry={ deletedEntry => {
                                    notifyEntryDelete(deletedEntry, shortTermAssets, setShortTermAssets);
                                }}
                            />
                        );
                    }
                )
            }
            <div className={'flex w-100 items-end sub-group-row'}>
                {en.longTermAssets}
            </div>
            {
                longTermAssets && longTermAssets.map(
                    entry => {
                        return (
                            <BalanceSheetEntryComponent key={entry.description} entry={entry} currency={currency}
                                onUpdateEntry={ updatedEntry => {
                                    notifyEntryUpdate(updatedEntry, longTermAssets, setLongTermAssets);
                                }}
                                onDeleteEntry={ deletedEntry => {
                                    notifyEntryDelete(deletedEntry, longTermAssets, setLongTermAssets);
                                }}
                            />
                        );
                    }
                )
            }
            <BalanceSheetTotalComponent
                key={'total-assets'}
                description={en.totalAssets}
                amount={totalAssets}
                currency={currency}
            />

            <br/>
            <div className={'flex w-100 items-end total-row'}>
                {en.liabilities}
            </div>
            <div className={'flex w-100 sub-group-row'}>
                <div className={'w-third tl'}>{en.shortTermLiabilities}</div>
                <div className={'w-third tc'}>{en.monthlyPayment}</div>
                <div className={'w-third'}>&nbsp;</div>
            </div>
            {
                shortTermLiabilities && shortTermLiabilities.map(
                    entry => {
                        return (
                            <BalanceSheetEntryComponent key={entry.description} entry={entry} currency={currency}
                                onUpdateEntry={updatedEntry => {
                                    notifyEntryUpdate(updatedEntry, shortTermLiabilities, setShortTermLiabilities);
                                }}
                                onDeleteEntry={ deletedEntry => {
                                    notifyEntryDelete(deletedEntry, shortTermLiabilities, setShortTermLiabilities);
                                }}
                            />
                        );
                    }
                )
            }
            <div className={'flex w-100 items-end sub-group-row'}>
                <div className={'w-65'}>{en.longTermDebt}</div>
                <div className={'w-10'}></div>
                <div className={'w-25 tr'}></div>
            </div>
            {
                longTermLiabilities && longTermLiabilities.map(
                    entry => {
                        return (
                                <BalanceSheetEntryComponent key={entry.description} entry={entry} currency={currency}
                                    onUpdateEntry={ updatedEntry => {
                                        notifyEntryUpdate(updatedEntry, longTermLiabilities, setLongTermLiabilities);
                                    }}
                                    onDeleteEntry={ deletedEntry => {
                                        notifyEntryDelete(deletedEntry, longTermLiabilities, setLongTermLiabilities);
                                    }}
                                />
                        );
                    }
                )
            }
            <BalanceSheetTotalComponent
                key={'total-liabilities'}
                description={en.totalLiabilities}
                amount={totalLiabilities}
                currency={currency}
            />
        </div>

    );
};

export default BalanceSheetComponent;