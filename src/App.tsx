import React from 'react';


import {availableCurrencies, balanceSheet} from './data';
import * as en from './en.json';

import BalanceSheetComponent from './components/BalanceSheetComponent';

function App() {
  return (
    <div className="App">
        <header className="balance-sheet-header">
            <div className="flex-column w-50-l w-75-m w-100-s center balance-sheet">
                <div className="flex w-100 items-end">
                    <div className="w-100 mt4 mb1">{ en.mainTitle }</div>
                </div>
            </div>
        </header>
        <nav>
        </nav>
        <main className={'pb6'}>
            <BalanceSheetComponent balanceSheet={balanceSheet} currencies={availableCurrencies}/>
        </main>
        <aside>

        </aside>
        <footer>
        </footer>
    </div>
  );
}

export default App;
