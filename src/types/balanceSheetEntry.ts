import BalanceSheetEntryType from './balanceSheetEntryType';

export interface BalanceSheetEntry {
    description: string;
    currency: string;
    amount: number;
    monthly?: number;
    type?: BalanceSheetEntryType;
}

export default BalanceSheetEntry;



