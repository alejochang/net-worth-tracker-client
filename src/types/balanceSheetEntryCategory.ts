enum BalanceSheetEntryCategory {
    ASSET = 'ASSET',
    LIABILITY = 'LIABILITY'
};

export const getTypedEntryCategory = (category: string) => BalanceSheetEntryCategory[category];

export default BalanceSheetEntryCategory;