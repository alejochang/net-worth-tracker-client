import BalanceSheetEntry from './balanceSheetEntry';

interface BalanceSheet {
    currency: string;
    assets: BalanceSheetEntry[];
    liabilities: BalanceSheetEntry[];
};
export default BalanceSheet;
