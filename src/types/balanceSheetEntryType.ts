enum BalanceSheetEntryType {
    SHORT_TERM = 'SHORT_TERM',
    LONG_TERM = 'LONG_TERM'
};

export const getTypedEntryType = (type: string) => BalanceSheetEntryType[type];

export default BalanceSheetEntryType;