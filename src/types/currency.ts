interface Currency {
    isoCode: string,
    symbol: string
};

export const USD: Currency = {
    isoCode: 'USD',
    symbol: 'US$'
};

export default Currency;