import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import BalanceSheet from '../types/balanceSheet';

const serviceApi = (serviceUrl: string) => {

    const headers: AxiosRequestConfig = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Access-Control-Allow-Origin': '*'
        }
    };

    const calculateNetWorth = async (balanceSheet: BalanceSheet) => {
        const response: AxiosResponse = await axios.put(`${serviceUrl}/totals`, {
            assets: balanceSheet.assets,
            liabilities: balanceSheet.liabilities,
            currency: balanceSheet.currency
        }, headers);
        return response.data;
    };

    const convertBalanceSheet = async (balanceSheet: BalanceSheet, currency: string) => {
        const response: AxiosResponse = await axios.post(`${serviceUrl}/convert/${currency}`, {
            assets: balanceSheet.assets,
            liabilities: balanceSheet.liabilities,
            currency: balanceSheet.currency
        }, headers);
        return response.data;
    }

    return {
        calculateNetWorth,
        convertBalanceSheet
    }
};

export default serviceApi('http://localhost:8080/balancesheet');