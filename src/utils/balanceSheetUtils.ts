import BalanceSheetEntry from '../types/balanceSheetEntry';
import BalanceSheetEntryType from '../types/balanceSheetEntryType';
import BalanceSheet from '../types/balanceSheet';
import Currency, {USD} from '../types/currency';

export const filterByType = (entries: BalanceSheetEntry[], type: BalanceSheetEntryType): BalanceSheetEntry[] => {
    return entries ? entries.filter(entry => entry.type === type) : [];
};

export const loadBalanceSheet = (balanceSheet: BalanceSheet, currencies: Currency[]) => {
    const shortTermAssets: BalanceSheetEntry[] = filterByType(balanceSheet.assets, BalanceSheetEntryType.SHORT_TERM);
    const longTermAssets: BalanceSheetEntry[] = filterByType(balanceSheet.assets, BalanceSheetEntryType.LONG_TERM);
    const shortTermLiabilities: BalanceSheetEntry[] = filterByType(balanceSheet.liabilities, BalanceSheetEntryType.SHORT_TERM);
    const longTermLiabilities: BalanceSheetEntry[] = filterByType(balanceSheet.liabilities, BalanceSheetEntryType.LONG_TERM);
    const matchedCurrency = currencies.find(cur => cur.isoCode === balanceSheet.currency);
    return {
        shortTermAssets,
        longTermAssets,
        shortTermLiabilities,
        longTermLiabilities,
        currency: matchedCurrency ? matchedCurrency : USD
    };
};

export const getNumberFormatOptionsByCurrency = (currency: Currency) => {
    const isoCode = currency.isoCode;
    if(['GBP', 'EUR'].includes(isoCode)){
        return ({
            thousandSeparator: '.',
            decimalSeparator: ','
        });
    } else{
        return ({
            thousandSeparator: ',',
            decimalSeparator: '.'
        });
    }

};