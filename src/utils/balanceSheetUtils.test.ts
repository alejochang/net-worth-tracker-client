import { loadBalanceSheet } from './balanceSheetUtils';
import BalanceSheet from '../types/balanceSheet';
import BalanceSheetEntryType from '../types/balanceSheetEntryType';


describe('balanceSheetUtils', () => {
    const currencies = [{ isoCode: 'CAD', symbol: '$'},
                        { isoCode: 'BTC', symbol: '₿'},
                        { isoCode: 'ETH', symbol: 'Ξ'}];
    describe('loadBalanceSheet returns an object with:', () => {
        let balanceSheet: BalanceSheet;
        beforeEach(()=> {
            balanceSheet = {
                currency: 'CAD',
                assets: [{
                    description: "Chequing",
                    currency: 'USD',
                    amount: 2000,
                    type: BalanceSheetEntryType.SHORT_TERM
                },
                {
                    description: "Home",
                    currency: 'USD',
                    amount: 1000000,
                    type: BalanceSheetEntryType.LONG_TERM
                }],
                liabilities: [{
                    description: "Investment Loan",
                    currency: 'USD',
                    amount: 100000,
                    type: BalanceSheetEntryType.LONG_TERM
                },
                {
                    description: "Mortgage",
                    currency: 'USD',
                    amount: 500000,
                    type: BalanceSheetEntryType.LONG_TERM
                }]
            }
        });
        it('shortTermAssets', () => {
            const { shortTermAssets } = loadBalanceSheet(balanceSheet, currencies);
            expect(shortTermAssets).toEqual([{
                    description: "Chequing",
                    currency: 'USD',
                    amount: 2000,
                    type: BalanceSheetEntryType.SHORT_TERM
                }]);
        });
        it('longTermAssets', () => {
            const { longTermAssets } = loadBalanceSheet(balanceSheet, currencies);
            expect(longTermAssets).toEqual([{
                description: "Home",
                currency: 'USD',
                amount: 1000000,
                type: BalanceSheetEntryType.LONG_TERM
            }]);
        });
        it('shortTermLiabilities', () => {
            const { shortTermLiabilities } = loadBalanceSheet(balanceSheet, currencies);
            expect(shortTermLiabilities).toEqual([]);
        });
        it('longTermLiabilities', () => {
            const { longTermLiabilities } = loadBalanceSheet(balanceSheet, currencies);
            expect(longTermLiabilities).toEqual([{
                    description: 'Investment Loan',
                    currency: 'USD',
                    amount: 100000,
                    type: BalanceSheetEntryType.LONG_TERM
                },
                {
                    description: 'Mortgage',
                    currency: 'USD',
                    amount: 500000,
                    type: BalanceSheetEntryType.LONG_TERM
                }]);
        });
        it('currency', () => {
            const { currency } = loadBalanceSheet(balanceSheet, currencies);
            expect(currency).toEqual(currencies[0]);
        });
    });
});