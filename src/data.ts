import BalanceSheet from './types/balanceSheet';
import BalanceSheetEntryType from './types/balanceSheetEntryType';
import Currency, {USD} from './types/currency';


export const balanceSheet: BalanceSheet = {
  currency: 'USD',
  assets: [
      {
        description: "Chequing",
        currency: 'USD',
        amount: 2000,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Savings for Taxes",
        currency: 'USD',
        amount: 4000,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Rainy Day Fund",
        currency: 'USD',
        amount: 506,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Savings for Fun",
        currency: 'USD',
        amount: 5000,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Savings for Travel",
        currency: 'USD',
        amount:400,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Savings for Personal Development",
        currency: 'USD',
        amount: 200,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Investment 1",
        currency: 'USD',
        amount: 5000,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Investment 2",
        currency: 'USD',
        amount: 60000,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Investment 3",
        currency: 'USD',
        amount: 3000,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Investment 4",
        currency: 'USD',
        amount: 50000,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Investment 5",
        currency: 'USD',
        amount:24000,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Primary Home",
        currency: 'USD',
        amount:455000,
        type: BalanceSheetEntryType.LONG_TERM
      },
      {
        description: "Second Home",
        currency: 'USD',
        amount:1564321,
        type: BalanceSheetEntryType.LONG_TERM
      },
      {
        description: "Other",
        currency: 'USD',
        amount: 0,
        type: BalanceSheetEntryType.LONG_TERM
      }
    ],
  liabilities: [
      {
        description: "Credit Card 1",
        currency: 'USD',
        monthly: 200,
        amount: 4342,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Credit Card 2",
        currency: 'USD',
        monthly: 150,
        amount: 322,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "others",
        currency: 'USD',
        amount: 0,
        type: BalanceSheetEntryType.SHORT_TERM
      },
      {
        description: "Mortgage 1",
        currency: 'USD',
        monthly: 2000,
        amount: 250999,
        type: BalanceSheetEntryType.LONG_TERM
      },
      {
        description: "Mortgage 2",
        currency: 'USD',
        monthly: 3500,
        amount: 632634.89,
        type: BalanceSheetEntryType.LONG_TERM
      },
      {
        description: "Line of Credit",
        currency: 'USD',
        monthly:500,
        amount: 10000,
        type: BalanceSheetEntryType.LONG_TERM
      },
      {
        description: "Investment Loan",
        currency: 'USD',
        monthly: 700,
        amount: 10000,
        type: BalanceSheetEntryType.LONG_TERM
      },
      {
        description: "Student Loan",
        currency: 'USD',
        amount: 0,
        type: BalanceSheetEntryType.LONG_TERM
      },
      {
        description: "Car Loan",
        currency: 'USD',
        amount:0,
        type: BalanceSheetEntryType.LONG_TERM
      }
    ]
};



export const availableCurrencies: Currency[] = [
    USD,
    {
        isoCode: 'CAD',
        symbol: 'C$'
    },
    {
        isoCode: 'GBP',
        symbol: '£'
    },
    {
        isoCode: 'EUR',
        symbol: '€'
    },
    {
        isoCode: 'JPY',
        symbol: '¥'
    },
    {
        isoCode: 'AUD',
        symbol: 'A$'
    },
    {
        isoCode: 'CHF',
        symbol: 'CHF'
    },
    {
        isoCode: 'CNY',
        symbol: '元'
    },
    {
        isoCode: 'HKD',
        symbol: 'HK$'
    },
    {
        isoCode: 'NZD',
        symbol: 'NZ$'
    }
    // ,{
    //     isoCode: 'BTC',
    //     symbol: '₿'
    // },
    // {
    //     isoCode: 'ETH',
    //     symbol: 'Ξ'
    // }
];